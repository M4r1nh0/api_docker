from flask import Flask
from datetime import datetime as dt
import cv2
import base64

app = Flask(__name__)


#captura = cv2.VideoCapture(0) # camera local
captura = cv2.VideoCapture('http://114.179.205.142/') # camera externa


def get_img_camera():
  ret, frame = captura.read()
  if ret == True:
    im_bytes = frame.tobytes()
    im_b64 = base64.b64encode(im_bytes)
  if ret != True:
    im_b64 = "Not found"
  return im_b64


@app.route('/api/camera/captura/') # rota para pegar img de camera
def captura_img():
  imgbs64 = get_img_camera()
  return {"status":str(dt.now()),"imgbs4": imgbs64}


@app.route('/') # checka status
def status_api():
  return {"status":str(dt.now()), "msg":"WORK!"}

# if __name__ == '__main__':
   # app.run(host="0.0.0.0", port=80, debug=True)
